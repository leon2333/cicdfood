[![pipeline status](https://gitlab.com/leon2333/cicdfood/badges/master/pipeline.svg)](https://gitlab.com/leon2333/cicdfood/commits/master)

[![coverage report](https://gitlab.com/leon2333/cicdfood/badges/master/coverage.svg)](https://gitlab.com/leon2333/cicdfood/badges/master/coverage.svg?job=coverage)

## FoodHub Web API.

# Assignment 2 - Agile Software Practice.

Name: Ang Li
Student Id:20086443

## Client UI.
>>Home page 
![][Home]
>>Allow users to buy products and pick them into their shopping cart
![][products]
>>Allow users share foods with other users
![][ShareEssay]
>>Allow users to watch video there
![][video]
>>Allow users read the essays wirtted by others
![][essays]
>>Allow user to find the delicious foods and local restaurants here
![][map]
>>Allow user to comment for essays and read others' comments
![][comments]
>>Allow users to check the products they choose
![][order]
>>Allow users to see the foods there and they can give like for the food they like
![][foods]



## E2E/Cypress testing.

I user the Cypress testing to test the components in the interface and I also test for the functions the user can use in this website.


## Web API CI.

https://leon2333.gitlab.io/cicdfood/coverage/lcov-report/

## links.
Client GitLab repo: https://gitlab.com/leon2333/cicdfood
Client Staging URL:https://foodhub-api-staging.herokuapp.com/
Client Production URL:https://foodhub-api-prod.herokuapp.com/

Web API GitLab repo:https://gitlab.com/leon2333/web-staging
Web API Staging URL:https://web-staging-foodhub.herokuapp.com/
Web API Production URL:https://web-prod-foodhub.herokuapp.com/


[foodhub]: ./img/donate.png
[Home]: ./img/Home.png
[products]: ./img/products.png
[ShareEssay]: ./img/ShareEssay.png
[video]: ./img/video.png
[essays]: ./img/eaaays.png
[map]: ./img/map.png
[comments]: ./img/comments.png
[order]: ./img/order.png

[foods]: ./img/foods.png
